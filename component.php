<?php

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault()) {$isHome=true;}else{$isHome=false;}
// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js');
// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/font-awesome.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/animate.min.css');
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');
// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<a class="navbar-brand text-center" href="/"><img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" /></a>';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<a class="navbar-brand text-center" href="/">' . htmlspecialchars($this->params->get('sitetitle')) . '</a>';
}
else
{
	$logo = '<a class="navbar-brand text-center" href="/">' . $sitename . '</a>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
	<jdoc:include type="head" />
    <!--[if lt IE 9]>
    <script src="<? echo $this->baseurl . '/templates/' . $this->template ?>/js/html5shiv.js"></script>
    <script src="<? echo $this->baseurl . '/templates/' . $this->template ?>/js/respond.min.js"></script>
    <![endif]-->       

    <?php // Template color ?>
	<?php if ($this->params->get('templateColor')) : ?>
	<style type="text/css">
		body.site
		{
			border-top: 3px solid <?php echo $this->params->get('templateColor'); ?>;
			background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>
		}
		a
		{
			color: <?php echo $this->params->get('templateColor'); ?>;
		}
		.navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus, .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus 
 .navbar-inverse .navbar-nav > li.parent:hover > a, .navbar-inverse .navbar-nav > li.parent:hover > a:hover, .navbar-inverse .navbar-nav > li.parent:hover > a:focus,#director_message,.navbar-inverse .navbar-nav .dropdown-menu,
		.navbar-inverse, #footer
		{
			background: <?php echo $this->params->get('templateColor'); ?> !important;
		}
		
	</style>
	<?php endif; ?>
	<!--[if lt IE 9]>
		<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '');
?>">
			<!-- Header -->
	<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php echo $logo; ?>
            </div>
            <div class="collapse navbar-collapse">
                <?php if ($this->countModules('position-1')) : ?>
				<nav class="navbar-nav navbar-right" role="navigation">
					<jdoc:include type="modules" name="position-1" style="none" />
				</nav>
			<?php endif; ?>
            </div>
        </div>
	</header>
	<section id="top_content_header">
		<?php if ($this->params->get('email')) : ?>
			<?php echo '<section class="phone"><a class="email_link" href="'. htmlspecialchars($this->params->get('email_link')) .'">' . htmlspecialchars($this->params->get('email')) . '</a></section>'; ?>
	 	<?php endif; ?>
		<?php if ($this->params->get('phone')) : ?>
			<?php echo '<section class="phone">' . htmlspecialchars($this->params->get('phone')) . '</section>'; ?>
	 	<?php endif; ?>
		<?php if ($this->params->get('address')) : ?>
			<?php echo '<section class="phone">' . htmlspecialchars($this->params->get('address')) . '</section>'; ?>
	 	<?php endif; ?>
        <?php if ($this->params->get('address')) : ?>
			<?php echo '<a href="' . htmlspecialchars($this->params->get('fb_link')) . '"><img class="pull-right fb_icon hidden-md hidden-sm hidden-xs" src="'.$this->baseurl . '/templates/' . $this->template . '/images/site/fb-icon.png" alt=""></a>'; ?>
	 	<?php endif; ?>
    </section>
  <div id="main-ky-container" class="container">
      <div class="row">
        <div class="col-md-12">
					<jdoc:include type="message" />
					<jdoc:include type="component" />
        </div>
    </div>
</div>
  <section class="bottom-nav text-right">
		<?php if ($this->params->get('email')) : ?>
			<?php echo '<a class="email_link" href="'. htmlspecialchars($this->params->get('email_link')) .'">' . htmlspecialchars($this->params->get('email')) . '</a>'; ?>
	 	<?php endif; ?>
		<?php if ($this->params->get('phone')) : ?>
			<?php echo '<a class=" email_link">' . htmlspecialchars($this->params->get('phone')) . '</a>'; ?>
	 	<?php endif; ?>
    </section>
  <a class="btn btn-social-icon btn-twitter">
    <i class="fa fa-twitter"></i>
  </a>
 <div class="navbar navbar-default navbar-fixed-bottom" id="footer">
    <div class="container">
     <div class="row">
               <div class="col-sm-6">
                     <a class="hidden-xs copyright-footer" target="_blank" href="http://www.armando.sx" title="Website designed and deployed by Armando.sx"> &copy; 2015 Kingdom Youths Inc.</a>
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
					<?php if ($this->countModules('position-2')) : ?>
                          <jdoc:include type="modules" name="position-2" style="none" />
                                <?php endif; ?>
                        <li><a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></li><!--#gototop-->
                    </ul>
                </div>
            </div>
    </div> 
</div>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.js"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/main.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo  htmlspecialchars($this->params->get('analytics'))?>', 'auto');
  ga('send', 'pageview');

</script>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>